<?php
 session_start();
 require("../config/database.php");
 require("../includes/functions.php");

extract($_POST);

$q = $db->prepare("SELECT email , pseudo, name ,avatar FROM users
                      WHERE (name LIKE :query OR email LIKE :query OR avatar LIKE :query) LIMIT 5");
$q->execute(
	[ 
	    'query' =>'%'.$query.'%'
	 ]
	);
$users = $q->fetchAll(PDO::FETCH_OBJ);
foreach ($users as $user) {
?>

    <div class="display-box-user">
        <a href="profile.php?id=<?= $user->id ?>" >
        	<img src="<?= $user->avatar ? $user->avatar:
                      get_avatar_url($user->email,30) ?>" width ="25" 
                      alt="<?= echappe($user->pseudo) ?>" class="img-circle" width="25">&nbsp;
        	<?= echappe($user->name) ?><br>
	        <?= echappe($user->email) ?>
        </a>
    </div>
<?php	

	
}
?>
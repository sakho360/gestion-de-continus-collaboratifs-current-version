
<?php $tilte='Inscription';?>


<?php include("partials/_header.php"); ?>
    
    
    <div class="main-content">
         
         <div class="container">

          <h1 class="lead">Devenez deja present membre!</h1>

          <?php
                    include("partials/_error.php");
          ?>

          <form data-parsley-validate  method="post" class="well col-md-6"  >

            <!--  name field  -->
            <div class="form-group">
              <label class="control-label" for="name"><i class="fa fa-edit"></i> Nom:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('name') ?>" type="text" name="name" id="name" required="required">
            </div>

             <!-- pseudo field  -->
            <div class="form-group">
              <label class="control-label" for="pseudo"><i class="fa fa-user"></i> Pseudo:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('pseudo') ?>" type="text" name="pseudo" id="pseudo" required="required" data-parsley-minlength="3">
            </div>

             <!-- email field -->
            <div class="form-group">
              <label class="control-label" for="email"><i class="fa fa-envelope"></i> Adresse Email:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('email') ?>" type="email" name="email" id="email" required="required" data-parsley-trigger="keypress">
            </div>

              <!-- password field  -->
            <div class="form-group">
              <label class="control-label" for="password"><i class="fa fa-lock"></i> Mot de Passe:</label>
              <input class="form-control" type="password" name="password" id="password" required="required">
            </div>

             <!-- Fconfirmation field  -->
            <div class="form-group">
              <label class="control-label" for="password_confirm"><i class="fa fa-lock"></i> Confirmer votre mot de passe:</label>
              <input class="form-control" type="password" name="password_confirm" id="password_confirm" required="required" data-parsley-equalto="#password">
            </div>
            
            <input class="btn btn-primary" type="submit" name="register" value="Inscription">

          </form>

         </div>  

    </div>
    

     <?php include("partials/_footer.php"); ?>

 
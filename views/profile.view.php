
<?php $tilte='Page de profile';?>


<?php include("partials/_header.php"); ?>
    
    
    <div class="main-content">
         
        <div class="container">
            <div class="row">
            	<div class="col-md-6">

            	    <div class="panel panel-default">
		                <div class="panel-heading">
		                	<h3 class="panel-title">Profil de <?=echappe($_SESSION['pseudo']) ?>
                      (<?= friends_count($_GET['id'])?> ami<?= friends_count($_GET['id'])==1 ?'' :'s' ?> )</h3>
		                </div>
		                <div class="panel-body">
                          
                        <div class="row">
                           <div class="col-md-5">
                           	 <img src="<?= $user->avatar ? $user->avatar:
                              get_avatar_url($user->email,100) ?>" width ="70" height="70"
                           	 	alt ="image de profil de <?=echappe($user->pseudo) ?>"
                               class= "img-circle">
                           </div>

                            <div col-md-7>
                            <?php if(!empty($_GET['id']) && $_GET['id']!==get_session('id_user')):?>
                              <?php require("partials/_relation_link.php") ; ?>
                            <?php endif;?>  
                           </div>
                           </div>

                           <div class="row">
                           	 <div class="col-sm-6">
                           	 <strong><?= echappe($user->pseudo); ?></strong></br>
                           	 <a href="mailto:<?= echappe($user->email)?>"> 
                           	  <?= echappe($user->email) ;?> </a></br>
                           	   <?=
                           	      $user->city && $user->country ?
                           	      '<i class="fa fa-location-arrow"></i>&nbsp;'
                           	      .echappe($user->city).'-'.echappe($user->country).'</br>'
                           	      : '' ;
                           	    ?><a href="https://www,google.com/maps?q=<?=echappe($user->city).' '.echappe($user->country) ?> " target= "_blank">Voir sur google maps</a>
                           	  
                           	 	
                           	 </div>
                           	 <div class="col-sm-6">
                           	    <?=
                           	        $user->twitter ? 
                           	        '<i class="fa fa-twitter"></i>&nbsp; 
                           	         <a href="//twitter.com/'.echappe($user->twitter).'">@'.echappe($user->twitter).'</a></br>' : '';
                           	    ?>
                           	    <?=
                           	        $user->github ? 
                           	        '<i class="fa fa-github"></i>&nbsp;
                           	         <a href="//github.com/'.echappe($user->github).'">'.echappe($user->github).'</a></br>' : '';
                           	    ?>
                           	    <?=
                           	       $user->sexe == 'H' ? 
                           	       '<i class="fa fa-male"></i>'
                           	       : '<i class="fa fa-female"></i>';

                           	    ?>
                           	    <?=
                           	       $user->avaible_for_hiring ? 
                           	        'disponible pour emploi'
                           	       :'Non disponible pour emploi';

                           	    ?>
                           	 	
                           	 </div>
                           </div>
                           <div class="row">
                           	<div class="col-md-12 well">
                           		<h5>Petite biographie de <?= echappe($user->name);?></h5>
                           		<p>
                           			<?=
                           			  $user->bio ? nl2br(echappe($user->bio)) : 'Aucune biographie pour le moment' ;
                           			?>

                           		</p>
                           	</div>
                           </div>
		                </div>
                    </div>   
            		
            	</div>

            	<div class="col-sm-6">
                <?php if(!empty($_GET['id']) && $_GET['id']==get_session('id_user')): ?>
                  <div class="status-post"> 
                   <form  action="microposts.php" method="post" data-parsley-validate>
                      <div class="form-group">
                        <label class="sr-only" for="content">Statut:</label>
                        <textarea class="form-control" name="content" id="content"
                         rows="3" required="required" maxlength="140" minlength="3" placeholder="Alors quoi de neuf ?" >  
                         </textarea>
                      </div>
                      <div class="form-group status-post-submit">
                        <input type="submit" name="publier" value="Publier" class="btn btn-success">
                      </div>
                   </form>
                  </div> 
                <?php endif; ?>

                 <?php if (count($microposts)!=0) :?> 
                  
                     <?php foreach ($microposts as $micropost) :?>
                       <?php include('partials/_micropost.php');?>
                     <?php endforeach; ?>
                 <?php else: ?>
                 <p>Cet utilisateur n'a encore rien poste pour le moment...</p>      
                 <?php endif; ?> 
              </div>

            </div>

          
          <?php
                    include("partials/_error.php");
          ?>

         </div>  

    </div>
    

    
 
    <script src="assets/js/jquery.min.js"></script>
    <script  src="librairies/sweetalert/sweetalert.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="librairies/uploadify/jquery.uploadify.min.js"></script>
    <script src="librairies/parsley/parsley.min.js"></script>
    <script src="librairies/parsley/i18n/fr.js"></script>
    <script src="assets/js/jquery.timeago.js"></script>
    <script src="assets/js/jquery.timeago.fr.js"></script>
    
    
    <script type="text/javascript">
      window.ParsleyValidator.setLocale('fr')
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
        $(".timeago").timeago();
      });
        window.ParsleyValidator.setLocale('fr') 
    </script>
    
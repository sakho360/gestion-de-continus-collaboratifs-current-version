
<?php $tilte='Connexion';?>


<?php include("partials/_header.php"); ?>
    
    
    <div class="main-content">
         
         <div class="container">

          <h1 class="lead">Connexion</h1>

          <?php
                    include("partials/_error.php");
          ?>

          <form data-parsley-validate  method="post" class="well col-md-6"  >

            <!--  identifiant field  -->
            <div class="form-group">
              <label class="control-label" for="identifiant"><i class="fa fa-user"></i> Pseudo ou Adresse electronique:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('identifiant') ?>" type="text" name="identifiant" id="identifiant" required="required">
            </div>

              <!-- password field  -->
            <div class="form-group">
              <label class="control-label" for="password"><i class="fa fa-unlock"></i> Mot de Passe:</label>
              <input class="form-control" type="password" name="password" id="password" required="required">
            </div>

             <!-- Remember me field  -->
            <div class="form-group">
              <label class="control-label" for="remember_me">
                <input type="checkbox" name="remember_me" id="remember_me">
                Garder ma session active
              </label>
              
            </div>
            
            <input class="btn btn-primary" type="submit" name="login" value="Connexion">

          </form>

         </div>  

    </div>
    

     <?php include('partials/_footer.php'); ?>

 
<?php $title = "Inscription"; ?>
<?php

   // Pour pouvoir utliser $_SESSION  77 109 05 93
   session_start();
   require("includes/init.php");
   require("filters/auth_filter.php");
   

  if (!empty($_GET['id'])) {

    $data = find_code_by_id($_GET['id']);
    if (!$data) {

        $code = "";
    }else{

      $code = $data->code; 
    }

  }else{

    $code = "";
  }
  
  // le formulaire a ete soumis
   if (isset($_POST['save'])) {

       if (no_empty(['code'])) {
          extract($_POST);
          
          $query = $db->prepare("INSERT INTO codes(code) VALUES (:code) ");
          $success=$query->execute(['code'=>$code]);
          if ($success) {
            # Afficher le code source
            $id = $db->lastInsertId();
            redirection('show_code.php?id='.$id);
          }else{
            set_flash("Erreur lors de l'ajout du code source. Veuilez reessayer SVP!");
            redirection('share_code.php');
          }
       }else{
           redirection('share_code.php');
       }
   }
?>




<?php

    require("views/share_code.view.php");
?>

<?php
 session_start();
 require("includes/init.php");
 require("filters/auth_filter.php");
 

   // le formulaire a ete soumis
   if (isset($_POST['change_password'])) {

   	    $errors =[];

     	 // si tous les champs ont ete remplies
     	if (no_empty(['current_password','new_password','new_password_confirm']) )
        {
           extract($_POST);

           if (mb_strlen($new_password) < 6 ){
               $errors[] ="Mot de passe trop court (Minimum 6 caracteres)!";
            }else {

              if ($new_password!=$new_password_confirm) {
                $errors[]="Vos deux mots de passes ne concordent pas!";
                
              }
            }

            if (count($errors)==0) {

               $query= $db->prepare("SELECT password AS hashed_password FROM users
                                     WHERE id= :id  
                                     AND active='1' ");                 
               $query->execute(
                  [
                    'id'=>get_session('id_user'),
                        
                  ]);
                  
                // On compte le nombre de l'utilisateur trouves
                $user = $query->fetch(PDO::FETCH_OBJ) ;


                if ($user && bcrypt_verify_password($current_password,$user->hashed_password)) {

                    $query= $db->prepare("UPDATE users SET password =:password
                                          WHERE id =:id");

                    $query->execute(
                          [
                            'password'=>bcrypt_hash_password($new_password),
                            
                            'id'=>get_session('id_user')
                          ]);

                     set_flash("Felicitation, votre mot de passe ete mis a jour!");
                     redirection('profile.php?id='.get_session('id_user'));

                 }else{

                   garder_infos_saisis();
                   set_flash("Le mot de passe actuel indique est incorrect!");
                 }
              
            }
               
        }else{
            // On garde les donnees saisies dans champs inputs
            garder_infos_saisis();
            $erros[] = "Veuillez remplir tous les champs marques d'un (*)" ;
        }

      
    } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }
   require("views/change_password.view.php"); 
 